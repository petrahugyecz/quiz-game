# Quiz game for Elsa 3D maps

<img src="https://gitlab.com/petrahugyecz/quiz-game/uploads/f1daa369b0fe29aa118197bc5bda9b31/elsa_logo.png" width="220" height="120" /><br>

**Online maps, AR application, Geographic games** 

We offer a solution for more effective teacher work in the classroom. The boring curriculum becomes interesting, the children become more motivated and interested with the help of the Elsa 3D maps and games.
Geography lessons have never been so interesting, with Elsa’s digital atlases!

**Online maps and games**

3D interactive thematic maps for geography education in your browser. Just choose a continent map and use it!
Quiz games belong to 3D maps. Geographical and topographical names can be practiced on an outline map


:earth_americas: Website: https://elsa3dmap.com/
